node default {
  notify { "Oops Default! I'm ${facts['hostname']}": }
}
node 'win.node.consul' {
  include ::profile::base_windows
  include ::profile::dns::client
}
node /lin\d?.node.consul/ {
#  class { 'os_hardening': }
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::client
  include ::profile::webserver
}
node 'mon.node.consul' {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::server
  include ::profile::database
}
node 'dir.node.consul' {
  include ::profile::base_linux
  include ::profile::dns::client
  include ::profile::consul::server
  include ::profile::haproxy
}

node 'manager.node.consul' {
  include ::role::directory_server
  include ::profile::base_linux
  include ::profile::base_manager
}
