import requests
from urllib3.connectionpool import xrange


def newuser(nname, imagelink):

    name = {'user': nname, 'image': imagelink}
    new_user = requests.get('http://lin0/newuser.php', params=name)
    s = 'New user' + nname + ' Image link' + imagelink
    print(s)
    print(new_user.text)


def newuserfromrandomuser(number):
    for x in xrange(number):
        print(x)
        r = requests.get('https://randomuser.me/api/')
        person = r.json()
        name = person['results'][0]['name']['first'] + ' ' + person['results'][0]['name']['last']
        picture = person['results'][0]['picture']['large']

        newuser(name, picture)


newuserfromrandomuser(1)
