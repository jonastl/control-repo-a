# webserver med apache
  class profile::webserver {

# Installing apache
  class { 'apache':
  mpm_module => 'itk',
  }

  class { 'apache::mod::php': }

# Mysql client for accessing database remotely
  class {'::mysql::client':
  package_name    => 'mysql-client',
  package_ensure  => 'present',
  bindings_enable => true,
}
# Pulling bookface repository
  vcsrepo { '/var/www/bookface':
  ensure   => present,
  provider => git,
  source   => 'https://gitlab.com/jonastl/bookface-a.git',
}
# Removing index.html
  file { '/var/www/html/index.html':
    ensure => absent,
  }
# Copying bookface php code into production
  file { 'bookface':
    ensure  => 'directory',
    source  => '/var/www/bookface/ecode',
    path    => '/var/www/html',
    recurse => true,
  }
# Copying the config file for php
  file { '/var/www/html/config.php':
    ensure => file,
    source => '/var/www/bookface/config.php';
  }
}
