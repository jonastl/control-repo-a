# Haproxy balancer
class profile::haproxy {
  include ::haproxy
  $lin0  = dns_a(lin0)
  $lin1  = dns_a(lin1)
# Running "if" to ensure that DNS is fully loaded and lin0 got an IP. 
# When stack is created, DNS didnt fully load before other modules,
# so the "if" check ensures that DNS is deployed before the rest
# of the code can continue running.
  if $lin0[0] {

# Defining balancer with name, IP and port to listen to
  haproxy::listen { 'dir':
    collect_exported => false,
    ipaddress        => $::ipaddress,
    ports            => '80',
    mode             => 'tcp',
    options          => {
      'option'  => [
        'tcplog',
      ],
      'balance' => 'roundrobin',
    },
  }
# Defining members of balancer (lin0 and lin1)  
  haproxy::balancermember { 'lin0':
    listening_service => 'web0',
    ipaddresses       => $lin0,
    ports             => '80',
    options           => 'check',
  }
  haproxy::balancermember { 'lin1':
    listening_service => 'web1',
    ipaddresses       => $lin1,
    ports             => '80',
    options           => 'check',
    }
}
}
