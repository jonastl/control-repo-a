 # Database for bookface
class profile::database {

  $root_password    = lookup('database::root_pw')
  $bookface_user    = lookup('database::bookface_user')
  $bookface_user_pw = lookup('database::bookface_user_pw')
  $lin0             = dns_a(lin0)
  $lin1             = dns_a(lin1)
  $username         = 'jeanclaude@'
  $buffer           = '/*.*'
  $userlin1         = "${username}${lin1[0]}"
  $grandlin1        = "${userlin1}${buffer}"

  if $lin0[0] {

class { '::mysql::server':
  root_password           => $root_password,
  remove_default_accounts => true
}


# Creating Bookface database
mysql::db { 'bookface':
  user     => $bookface_user,
  password => $bookface_user_pw,
  host     => $lin0[0],
  grant    => ['SELECT', 'UPDATE', 'INSERT', 'CREATE'],
}

file_line { 'replace':
  notify  => Service['mysql'],
  path    => '/etc/mysql/my.cnf',
  replace => true,
  line    => 'bind-address = 0.0.0.0',
  match   => 'bind-address = 127.0.0.1'
}
# Creating second user. Value is the IP
mysql_user{ $userlin1 :
  ensure        => present,
  password_hash => '*4154D8E72DFD0B3889BE5B1A17EDB8C5580CEA0D'
}
# Granting lin1 privileges
mysql_grant{ $grandlin1 :
  ensure     => present,
  options    => ['GRANT'],
  privileges => ['SELECT', 'UPDATE', 'INSERT', 'CREATE'],
  # Privileges are valid at all tables 
  table      => '*.*',
  user       => $userlin1,
}


#Create the tables for the database if they are not there
exec { 'Initiate the tables':
  path    => '/usr/bin',
  command => 'curl http://lin0/createdb.php',
}

}
}

