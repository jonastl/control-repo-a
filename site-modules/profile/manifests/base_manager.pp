#
# profile::base_manager
#

class profile::base_manager {

  # Pip and python
  $manager_sw_pkg = lookup('base_manager::manager_sw_pkg')

# Inserting our SSH keys into root on manager
  ssh_authorized_key { 'iverhh@loginstud01':
    user    => 'root',
    type    => 'ssh-rsa',
    key     => lookup('base_manager::iver_key'),
    require => File['/root/.ssh'],
  }
  ssh_authorized_key { 'vladimir@vladimir-Lenovo-Y50-70':
    user    => 'root',
    type    => 'ssh-rsa',
    key     => lookup('base_manager::vlad_key'),
    require => File['/root/.ssh'],
  }
  ssh_authorized_key { 'jonastl@loginstud01 ':
    user    => 'root',
    type    => 'ssh-rsa',
    key     => lookup('base_manager::jonas_key'),
    require => File['/root/.ssh'],
  }
# Alias for deploying new configurations
  file_line { 'deploy alias':
    path => '/root/.bashrc',
    line => 'alias deploy="r10k deploy environment -pv && bash /etc/puppetlabs/code/environments/production/new_keys_and_passwds.bash && puppet agent -t"',
}

  package { $manager_sw_pkg:
    ensure => latest,
  }
# Important for our usergen script
  package { 'requests':
    ensure   => present,
    provider => 'pip',
    require  => Package['python-pip'],
  }

}
